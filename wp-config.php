<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'keptify');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'DKmJ~T&~e$|5h0qd8g1BM_MyEJN*%NI2#:z:!dl?{.#[v1(Df~mMI|?pBj!,4{63');
define('SECURE_AUTH_KEY',  '/!NWcRK*@^i/?b|m/WD<z0M+#jgm#a&?ai*D)waxlp=<li sVkuW.8x5B#^8(I1x');
define('LOGGED_IN_KEY',    'y]am.>8Yq JSYCk/<B$V/>|wEM/,Y&gx[iw6s#nuI6HXn;7`#mU;,FrT$~ayihnE');
define('NONCE_KEY',        'J$mRrw&]k|54tS!^33b5V9g$?G<ZSb=TYe|VaAA<#%1FK&T,1@DZ]?rn?1|od#_J');
define('AUTH_SALT',        'ZdIipEZ[!0EHgn+G/k{/0Em$G{E%}!PJC&]e`)&c*~o&nenSDA}9c;%;Ay(XQHiF');
define('SECURE_AUTH_SALT', '%lMG@e^[^uZ9K^A.B#6.L{kYU87(C4h%;|s?t{%>Je^yIWE MRzQaQ#^<>V7):(e');
define('LOGGED_IN_SALT',   ') <CE^ev! ][-tS^1Tiu<Ln@#SDxPjaLa=[t/ZKj_}Q>{d&x(vu1p5T6+}[y,uP!');
define('NONCE_SALT',       '!g52O&ovD.tcq#Xl;IEQNqTm_3h -`!LS2N_qx!,p(QI!bwnd$~`vtsR4?=w8OEt');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
