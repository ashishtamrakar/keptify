<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 Template name: About us
 */

get_header(); ?>

<div class="wrapper page-wrapper content-header about-top color-white">
    <div class="about-bg" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/about-bg.png);"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6  col-lg-offset-3 col-md-8  col-md-offset-2 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                <h2 class="page-title page-title-sm text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">About Us</h2>
                <div class="separator wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"></div>
                <div class="page-subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">Trusted by over 140,000 users worldwide </div>
                <div class="pager-title-content wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">We’re a young, dynamic and talented team that loves business owners and marketers. Our goal was to build software to help you maximize revenue by remarketing to interested customers without it being tedious, complicated or expensive. When you succeed, we succeed, which is why we invest personal attention with every business that works with us. We have a track record of increasing online sales for our businesses by 30%. </div>
                
                
                
                
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper about-team text-center">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8  col-sm-offset-2">
            	<h2 class="page-title text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">OUR TEAM</h2>
                 <div class="page-subtitle wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">You are in a good company</div>
                 <div class="separator separator-black wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"></div>
                 
                 
                 <div class="row flex-horiz-center">
                 	<div class="col-lg-3 col-md-4  col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.75s">
                    	<div class="member-item"><figure><img src="<?php echo get_template_directory_uri(); ?>/img/member-1.png"></figure>
                       <div class="member-name"> Roshan Bhattarai</div>
<div class="member-designation">Founder</div>
<div class="member-social">
	<a href="#"><span class="fa fa-linkedin"></span></a>|<a href="#"><span class="fa fa-twitter"></span></a>
</div></div>
                    </div>
                    <div class="col-lg-3 col-md-4  col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">
                    	<div class="member-item"><figure><img src="<?php echo get_template_directory_uri(); ?>/img/member-2.png"></figure>
                       <div class="member-name"> Raghab Dhakal</div>
<div class="member-designation">Seo Expert</div>
<div class="member-social">
	<a href="#"><span class="fa fa-linkedin"></span></a>|<a href="#"><span class="fa fa-twitter"></span></a>
</div></div>
                    </div>
                    <div class="col-lg-3 col-md-4  col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.05s">
                    	<div class="member-item"><figure><img src="<?php echo get_template_directory_uri(); ?>/img/member-3.png"></figure>
                       <div class="member-name"> Binaya Dhungana</div>
<div class="member-designation">Support</div>
<div class="member-social">
	<a href="#"><span class="fa fa-linkedin"></span></a>|<a href="#"><span class="fa fa-twitter"></span></a>
</div></div>
                    </div>
                    <div class="col-lg-3 col-md-4  col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
                    	<div class="member-item"><figure><img src="<?php echo get_template_directory_uri(); ?>/img/member-4.png"></figure>
                       <div class="member-name"> Madan Karki</div>
<div class="member-designation">Developer</div>
<div class="member-social">
	<a href="#"><span class="fa fa-linkedin"></span></a>|<a href="#"><span class="fa fa-twitter"></span></a>
</div></div>
                    </div>
                    
                    <div class="col-lg-3 col-md-4  col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.35s">
                    	<div class="member-item"><figure><img src="<?php echo get_template_directory_uri(); ?>/img/member-5.png"></figure>
                       <div class="member-name"> Sushil Adhikari
</div>
<div class="member-designation">Developer</div>
<div class="member-social">
	<a href="#"><span class="fa fa-linkedin"></span></a>|<a href="#"><span class="fa fa-twitter"></span></a>
</div></div>
                    </div>
                    <div class="col-lg-3 col-md-4  col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.5s">
                    	<div class="member-item"><figure><img src="<?php echo get_template_directory_uri(); ?>/img/member-6.png"></figure>
                       <div class="member-name">Bidur Maharjan</div>
<div class="member-designation">Developer</div>
<div class="member-social">
	<a href="#"><span class="fa fa-linkedin"></span></a>|<a href="#"><span class="fa fa-twitter"></span></a>
</div></div>
                    </div>
                 
                 </div>
                 
                 
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section-xs bg-red section-conversion wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="row">
        <div class="col-sm-6 text-right">
            <h3 class="section-title section-title-md title wt-bold spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">Start Converting</h3>
            <div class="font-md spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">Receive a personalised demo.</div>
        </div>
        <div class="col-sm-6"> <a href="#" class="btn btn-white-bordered wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri();?>/img/ico-play.png" class="ico"> Request a demo</a> </div>
    </div>
</div>
</div>
<?php

get_footer();

