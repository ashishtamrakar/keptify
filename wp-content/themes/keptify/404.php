<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package keptify
 */

get_header(); ?>

	<div id="primary" class="content-area content-section">
	<div class="container-fluid">	<main id="main" class="site-main row" role="main">

			<section class="error-404 not-found text-center col-sm-6 col-sm-offset-3">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'keptify' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'keptify' ); ?></p>
<div class="row"><div class=" col-sm-8 col-sm-offset-2"><form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<label>
					<span class="screen-reader-text">Search for:</span>
					<input type="search" class="search-field" placeholder="Search …" value="" name="s">
				</label>
				<div class="input-group-btn"><input type="submit" class="search-submit" value="Search"></div>
			</form></div></div>

					<?php
						//get_search_form();

						//the_widget( 'WP_Widget_Recent_Posts' );

						// Only show the widget if site has multiple categories.
						if ( keptify_categorized_blog() ) :
					?>

		

					<?php
						endif;

						/* translators: %1$s: smiley */
						$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'keptify' ), convert_smilies( ':)' ) ) . '</p>';
						//the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );

						//the_widget( 'WP_Widget_Tag_Cloud' );
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main --></div>
	</div><!-- #primary -->

<?php
get_footer();
