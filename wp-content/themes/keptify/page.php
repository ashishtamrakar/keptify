<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 */

get_header(); ?>
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); 
		 ?>
		 
		 
<div class="wrapper page-wrapper content-header">
	
    <div class="container-fluid">
    	<div class="row">
        	<div class="col-md-8 col-md-offset-2 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
            	<h2 class="page-title text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s"><?php the_title(); ?></h2>
                <div class="page-slogan text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">Built for marketers who understand that every dollar counts.</div>
                <div class="separator separator-black"></div>
            </div>
        </div>
    </div>

</div>

<div class="wrapper page-wrapper content-section compact editor-content">
	
    <div class="container-fluid">
    	<div class="row flex-center">
        	<div class="col-md-8  col-md-offset-2 col-sm-10  col-sm-offset-1 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
            	<?php the_content(); ?>
            </div>
        </div>
   </div>
</div>
<?php 
	} // end while
} // end if
?>
<?php
//get_sidebar();
get_footer();
