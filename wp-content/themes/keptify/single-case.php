<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package keptify
 */

get_header(); ?>

<div id="primary" class="content-area wrapper page-wrapper content-section  editor-content">
   
   
   
    <main id="main" class="site-main" role="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 ">
                    <?php
		while ( have_posts() ) : the_post();

			 ?>
             
             
                    <article class="">
                    <figure class="client-fig">
                    	<h2 class="client-name">Xiom Corp</h2>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/case-1w.png" class="case-cover-img">
                        <div class="case-logo"><img src="<?php echo get_template_directory_uri(); ?>/img/client-logo-1.png"></div>
                    </figure>
                        
                        
                        
                        
                        <div class="case-testimonial"><span class="fa fa-quote-left"></span>
                            <blockquote>
                            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                            </blockquote>
                            <div class="testimonial-author">Samuel Doe, Xiom Corp</div></div>
                        <div class="separator separator-black"></div>
                        <div class="entry-content">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing 
elit. Aenean commodo ligula eget dolor. Aenean massa. 
Cum sociis natoque penatibus et magnis dis parturient 
montes, nascetur ridiculus mus. Donec quam felis, 
ultricies nec, pellentesque eu, pretium quis, sem.</p>


<h3>Cum sociis natoque penatibus et magnis dis 
parturient montes nascetur ridiculus mus</h3>


<ul>
  <li>Lorem ipsum dolor sit amet, consectetuer adipiscing 
  elit. Aenean commodo ligula eget dolor. Aenean 
  massa.</li>
  <li>Cum sociis natoque penatibus et magnis dis 
  parturient montes, nascetur ridiculus mus. Donec quam 
  felis, ultricies nec, pellentesque eu, pretium quis, 
  sem.</li>
  <li>Nulla consequat massa quis enim. Donec pede justo, 
  fringilla vel, aliquet nec, vulputate eget, arcu.</li>
  <li>In enim justo, rhoncus ut, imperdiet a, venenatis 
  vitae, justo. Nullam dictum felis eu pede mollis 
  pretium. Integer tincidunt.</li>
</ul>


<blockquote>
Lorem ipsum dolor sit amet, consectetuer 
adipiscing elit. Aenean commodo ligula eget dolor. 
Aenean massa <strong>strong</strong>. Cum sociis 
natoque penatibus et magnis dis parturient montes, 
nascetur ridiculus mus. Donec quam felis, ultricies 
nec, pellentesque eu, pretium quis, sem. Nulla consequat 
massa quis enim. Donec pede justo, fringilla vel, 
aliquet nec, vulputate eget, arcu. In <em>em</em> 
enim justo, rhoncus ut, imperdiet a, venenatis vitae, 
justo. Nullam <a class="external ext" href="#">link</a>
dictum felis eu pede mollis pretium.
</blockquote>


<table class="data">
  <tr>
    <th>Entry Header 1</th>
    <th>Entry Header 2</th>
    <th>Entry Header 3</th>
    <th>Entry Header 4</th>
  </tr>
  <tr>
    <td>Entry First Line 1</td>
    <td>Entry First Line 2</td>
    <td>Entry First Line 3</td>
    <td>Entry First Line 4</td>
  </tr>
  <tr>
    <td>Entry Line 1</td>
    <td>Entry Line 2</td>
    <td>Entry Line 3</td>
    <td>Entry Line 4</td>
  </tr>
  <tr>
    <td>Entry Last Line 1</td>
    <td>Entry Last Line 2</td>
    <td>Entry Last Line 3</td>
    <td>Entry Last Line 4</td>
  </tr>
</table>

                        
                        
                        </div>
                    </article>
                    <?php endwhile; // End of the loop.
		?>
                </div>
                
                
                
            </div>
        </div>
    </main>
    <!-- #main --> 
</div>
<!-- #primary -->

<?php

get_footer();


