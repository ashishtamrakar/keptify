<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 Template name: FAQ
 */

get_header(); ?>

<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                <h2 class="page-title text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s">FAQ’s </h2>
                <div class="separator separator-black wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"></div>
                <div class="pager-title-content wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
                    <p>If you have a question, chances are we might have answered it already! Check out a list of frequently asked questions below, and if you still don’t see an answer to your question, you can:</p>
                    <ul>
                        <li>Submit a ticket</li>
                        <li>Live chat with one of our representatives</li>
                        <li>Schedule a demo call</li>
                    </ul>
                    <a href="#" class="btn btn-red btn-fixed-width wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s"><img src="<?php echo get_template_directory_uri(); ?>/img/ico-user.png" class="ico"> SIgn UP</a> </div>
                <div class="faq-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="faq-title">Contracts are scary, do I have to sign one?</h3>
                    <div class="faq-body"> Nope, try Keptify for one month and if you don’t like it, you can cancel anytime, just give us seven days notice. </div>
                </div>
                
                <div class="faq-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="faq-title">Will I be bothering customers with Keptify’s remarketing tools? 
</h3>
                    <div class="faq-body"> No, because we strategically target visitors who have already shown an interest in what you have to offer. Our remarketing emails have less than a .01% unsubscribe rate. </div>
                </div>
                
                <div class="faq-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="faq-title">How much money can I make using Keptify?</h3>
                    <div class="faq-body"> You can expect a 20-30% increase in sales. </div>
                </div>
                
                <div class="faq-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="faq-title">Who takes care of the design?</h3>
                    <div class="faq-body">We do, it’s a complimentary service we offer customers. We are committed to your success which is why our experts want to help optimize your campaigns. You are welcome to design your own, but the majority of our customers trust our expert team. </div>
                </div>
                
                <div class="faq-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="faq-title">Does Keptify work with any ecommerce platform?
</h3>
                    <div class="faq-body"> Absolutely. Our software is designed for maximum compatibility. We cover all the major ecommerce platforms like Shopify, Magento, OpenCart, Prestashop, 3dcart, Volusion, BigCommerce, but also any custom platform that our clients use. </div>
                </div>
                
                <div class="faq-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="faq-title">We already have a retargeting campaign. How is this different?
</h3>
                    <div class="faq-body"><p>Great question! Retargeting and remarketing are two separate things:</p>
<ul>
<li>Retargeting typically relies on displaying ads to customers on various sites as they surf the web. </li>
<li>Remarketing, on the other hand, uses email and occurs when you engage with visitors before they leave your website.</li> 
</ul></div>
                </div>
                
                
                <div class="faq-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="faq-title">Will this slow down my site?</h3>
                    <div class="faq-body"> Keptify will have absolutely no impact on the speed of your site. We intentionally designed the software with non-blocking technology to ensure that customers didn’t have to choose between our software and website speed. You can have it all!  </div>
                </div>
                
                <div class="faq-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="faq-title">How long will it take to get started?</h3>
                    <div class="faq-body"><strong class="color-red">48hrs...</strong>We don’t mess around! We know that every moment you don’t have Keptify you’re missing out on revenue. Once you add the Keptify tag on your site, our team of experts will start working on the setup and design of your campaign, which we complete within two days.  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section-xs bg-red section-conversion wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="row">
        <div class="col-sm-6 text-right">
            <h3 class="section-title section-title-md title wt-bold spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">Start Converting</h3>
            <div class="font-md spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">Receive a personalised demo.</div>
        </div>
        <div class="col-sm-6"> <a href="#" class="btn btn-white-bordered wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri();?>/img/ico-play.png" class="ico"> Request a demo</a> </div>
    </div>
</div>
</div>
<?php

get_footer();


