<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package keptify
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>
<?php if(!is_front_page()){ $bc='not-home';}else{$bc='';} ?>
<body <?php body_class($bc); ?>>
				
<header class="site-header wrapper fadeInDown">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2 col-xs-8">
                <h1 class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri();?>/img/logo.png" width="127"></a></h1>
            </div>
            <div class="col-sm-10 col-xs-4">
                <nav class="main-navigaion nav-menu" id="site-navigation">
                	<button class="nav-trigger nav-trigger-outer visible-sm visible-xs">
                    	<span class="line-1 line"></span>
                        <span class="line-2 line"></span>
                        <span class="line-3 line"></span>
                    </button>
                    <div class="menu-holder">
					<button class="nav-trigger nav-trigger-inner visible-sm visible-xs">
                    	<span class="line-1 line"></span>
                        <span class="line-2 line"></span>
                        <span class="line-3 line"></span>
                    </button>
					
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?></div>
                </nav>
            </div>
        </div>
    </div>
</header>
<div id="page" class="site wow fadeIn"  data-wow-duration="1s" data-wow-delay="0s">
<!-- #masthead -->

	<div id="content" class="site-content">
