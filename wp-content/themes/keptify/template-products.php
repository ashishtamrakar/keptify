<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 Template name: Products
 */

get_header(); ?>

<div class="wrapper page-wrapper content-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                <h2 class="page-title page-title-md text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">PRODUCTS TO KEPTIFY YOUR <br>
                    WEBSITE VISITORS </h2>
                <div class="separator separator-black wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"></div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-lg-4 col-lg-offset-2 col-md-5 col-md-offset-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title section-title-lg wt-thin">Cart Abandonment</h2>
                <div class="section-desc"> Understand why your website visitors aren’t buying with our analytics software. Discover abandonment trends and indicators such as location, gender, device, browser, search history, mouse movements, purchase history, and even your most abandoned products! Our analytics data will enable you to optimize your website to increase conversion. </div>
            </div>
            <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"> <img src="<?php echo get_template_directory_uri(); ?>/img/p-img-1.png" class="img-responsive"> </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section bg-lightgrey">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-lg-5 col-lg-offset-1  col-md-6 text-center wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"> <img src="<?php echo get_template_directory_uri(); ?>/img/p-img-2.png" class="img-responsive"> </div>
            <div class="col-lg-4  col-md-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title section-title-lg wt-thin">Cart Analyzer </h2>
                <div class="section-desc">
                    <p>No arbitrary data, but actionable details about your customer's behavior you can use to grow your business. Keptify offers real-time data about abandoning customers such as their device, location, and browser usage.</p>
                    <strong>Ex:</strong>
                    <p>John Doe from New York visited your site on an iPhone 6 Chrome browser, for 4 minutes. He came via Facebook ad link www.xyz.com and browsed your XYZ product for $200.</p>
                    <p>Identify the "conversion killers" on your site, and gain insight into visitors that convert well and ones that don't. Giving you the ability to create more targeted campaigns.</p>
                    <strong>Additional insights Keptify provides you with:</strong>
                    <ul>
                        <li> View the number of visitors on your site based on your customized date range. </li>
                        <li>Find out how many pages your website browsers have viewed (and which ones)</li>
                        <li>View a list of abandoned carts.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-lg-4 col-lg-offset-2 col-md-5 col-md-offset-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title section-title-lg wt-thin">Email Re-Marketing</h2>
                <div class="section-desc">
                    <p><b>Our all-encompassing remarketing software allows you to retarget customers in three ways:</b></p>
                    <p><span class="color-red">1.</span> Reminder emails sent immediately to an abandoning customer<br>
                        <span class="color-red">2.</span> Facebook Ad Network & Adroll<br>
                        <span class="color-red">3.</span> Lightbox form</p>
                    Keptify remarketing campaigns are completely customizable. Choose which pages you want features to appear on, as well as set rules to target customers based on purchase history, value, behavior, geographical location, and more! </div>
            </div>
            <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"> <img src="<?php echo get_template_directory_uri(); ?>/img/p-img-3.png" class="img-responsive"> </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section bg-lightgrey">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-lg-5 col-lg-offset-1  col-md-6 text-center wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"> <img src="<?php echo get_template_directory_uri(); ?>/img/p-img-4.png" class="img-responsive"> </div>
            <div class="col-lg-4  col-md-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title section-title-lg wt-thin">Email Automation </h2>
                <div class="section-desc">
                    <p>Avoid sending generic content with our advanced email marketing software. Automatically populate emails with relevant information to your customers based on their behavior. This is proven to increase customer retention, engagement, and email click-through rate.</p>
                    <ul>
                        <li>Send automated emails promoting new products that match their interest</li>
                        <li>Schedule newsletters to your current subscribers to keep them engaged</li>
                        <li>Send specific messages to specific groups of your email list using segmentation</li>
                    </ul>
                    <p>Once you implement our services, your business will begin to recoup thousands of dollars in possible missed revenue. Keptify plugs into any e-commerce website, and once it’s running, set it and forget it.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-lg-4 col-lg-offset-2 col-md-5 col-md-offset-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title section-title-lg wt-thin">Lightbox Form</h2>
                <div class="section-desc">
                    <p>Our software predicts the exact moment visitors are about to leave your website and triggers a lightbox giving you another chance to entice them to sign-up to your email list. This feature combined with a compelling message from your business will result in a 500% increase in daily email signups. Lightbox isn’t only for opt-ins, but also to motivate potential customers to complete their order r with a compelling offer such as a discount code. </p>
                    <ul>
                        <li>Customizable professional templates</li>
                        <li>Segmented lightbox display</li>
                        <li>A/B split testing</li>
                        <li>Multi-platform compatibility</li>
                    </ul>
                    <p>Keptify is unique in that our advanced remarketing tools are designed to target customers who have the highest probability of converting into a paying customer.</p>
                </div>
            </div>
            <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"> <img src="<?php echo get_template_directory_uri(); ?>/img/p-img-3.png" class="img-responsive "> </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section bg-lightgrey">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-lg-5 col-lg-offset-1  col-md-6 text-center wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"> <img src="<?php echo get_template_directory_uri(); ?>/img/p-img-5.png" class="img-responsive center-block"> </div>
            <div class="col-lg-4  col-md-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title section-title-lg wt-thin">Facebook & Adroll Remarketing </h2>
                <div class="section-desc">
                    <p>Remarket to customers using our software that integrates with Facebook Ads and Adroll. Use our analytics to deliver targeted ads to customers that have already been exposed to your message. Remarket to cart abandoners, customers from specific regions, previous buyers and more! These ads have a higher engagement because you're promoting to leads that already visited your website. </p>
                    <p> Facebook Ads or Adroll combined with our analytics gives you a powerful way to reach your audience.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section text-canter bg-container color-white text-center wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bg-dollar.png);">
    <h3 class="section-title wt-bold wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">Case Studies</h3>
    <div class="separator wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s"></div>
    <img class="case-img wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s" src="<?php echo get_template_directory_uri();?>/img/case-1.png">
    <div class="section-short-desc spacer-top-md wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">10:1 Return<br>
        on investment</div>
    <a href="#" class="btn btn-white-bordered wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri();?>/img/ico-download.png" class="ico"> Download Case Study</a></div>
<div class="wrapper page-wrapper content-section-xs bg-red section-conversion wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="row">
        <div class="col-sm-6 text-right">
            <h3 class="section-title section-title-md title wt-bold spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">Start Converting</h3>
            <div class="font-md spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">Receive a personalised demo.</div>
        </div>
        <div class="col-sm-6"> <a href="#" class="btn btn-white-bordered wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri();?>/img/ico-play.png" class="ico"> Request a demo</a> </div>
    </div>
</div>
</div>
<?php

get_footer();

