<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package keptify
 */

get_header(); ?>

<div id="primary" class="content-area wrapper page-wrapper content-section editor-content">
    <main id="main" class="site-main" role="main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-7 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                    <?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );
 ?>
			<div class="  wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s"><?php the_post_navigation(); ?></div>
<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
			 ?>	<div class="  wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s"><?php comments_template(); ?></div>
			<?php endif;
			?>
<?php  
		endwhile; // End of the loop.
		?>
                </div>
                <div class="col-md-3 col-md-offset-1 col-sm-5 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">
                <?php get_sidebar(); ?>
                <!--<section class="widget">
                    <h2 class="widget-title">Latest Posts</h2>
                    <div class="border-bottom sidebar-item font-sm latest-post-item">
                    	<a href="#" class="media">
                        	<div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/latest-post-1.png" class="media-object"></div>
                            <div class="media-body">
                            	<time class="text-muted spacer-bottom-sm">3 days ago</time>
                                <div class="latest-post-excerpt">Styled Themes – A new and improved site with ease in navigation!</div>
                            </div>
                        </a>
                    </div>
                    <div class="border-bottom sidebar-item font-sm latest-post-item">
                    	<a href="#" class="media">
                        	<div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/latest-post-1.png" class="media-object"></div>
                            <div class="media-body">
                            	<time class="text-muted spacer-bottom-sm">3 days ago</time>
                                <div class="latest-post-excerpt">Styled Themes – A new and improved site with ease in navigation!</div>
                            </div>
                        </a>
                    </div>
                    <div class="border-bottom sidebar-item font-sm latest-post-item">
                    	<a href="#" class="media">
                        	<div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/latest-post-1.png" class="media-object"></div>
                            <div class="media-body">
                            	<time class="text-muted spacer-bottom-sm">3 days ago</time>
                                <div class="latest-post-excerpt">Styled Themes – A new and improved site with ease in navigation!</div>
                            </div>
                        </a>
                    </div>
                </section>-->
                
                <section class="widget">
                    <h2 class="widget-title">Latest Comments</h2>
                    <div class="border-bottom sidebar-item font-sm latest-comment-item">
                    	<a href="#" class="media">
                        	<div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/author-1.png" class="media-object"></div>
                            <div class="media-body">
                            	<div class="row spacer-bottom-sm"><div class="author-name col-xs-6">Coco</div><time class="text-muted col-xs-6 text-right">April 7, 2016</time></div>
                                <div class="latest-post-excerpt"><em>"Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam…"</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="border-bottom sidebar-item font-sm latest-comment-item">
                    	<a href="#" class="media">
                        	<div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/author-1.png" class="media-object"></div>
                            <div class="media-body">
                            	<div class="row spacer-bottom-sm"><div class="author-name col-xs-6">Coco</div><time class="text-muted col-xs-6 text-right">April 7, 2016</time></div>
                                <div class="latest-post-excerpt"><em>"Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam…"</em></div>
                            </div>
                        </a>
                    </div>
                    <div class="border-bottom sidebar-item font-sm latest-comment-item">
                    	<a href="#" class="media">
                        	<div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/author-1.png" class="media-object"></div>
                            <div class="media-body">
                            	<div class="row spacer-bottom-sm"><div class="author-name col-xs-6">Coco</div><time class="text-muted col-xs-6 text-right">April 7, 2016</time></div>
                                <div class="latest-post-excerpt"><em>"Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam…"</em></div>
                            </div>
                        </a>
                    </div>
                                    </section>
            </div>
            </div>
        </div>
    </main>
    <!-- #main --> 
</div>
<!-- #primary -->

<?php

get_footer();

