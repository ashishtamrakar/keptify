<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 Template name: Case Study list
 */

get_header(); ?>

<div class="wrapper page-wrapper content-header">
	
    <div class="container-fluid">
    	<div class="row">
        	<div class="col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
            	<h2 class="page-title text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">Meet some of our customers</h2>
                <div class="page-slogan text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">From startups to established companies, our mission is to help you grow.</div>
                <div class="separator separator-black wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s"></div>
                
            </div>
        </div>
    </div>

</div>

<div class="wrapper page-wrapper content-section compact">
	
    <div class="container-fluid">
    	<div class="row">
        	<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            	<div class="case-item">
                	<figure class="client-fig wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    	<h2 class="client-name wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.4s">Xiom Corp</h2>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/case-1w.png" class="case-cover-img">
                        <div class="case-logo wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.6s"><img src="<?php echo get_template_directory_uri(); ?>/img/client-logo-1.png"></div>
                    </figure>
                    
                    
                    <div class="row case-body">
                    	<div class="case-excerpt col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
                        	<h3 class="title text-uppercase spacer-bottom-xs"><span class="fa fa-clipboard"></span> <span class="color-red">Details</span></h3>
                            <div class="case-excerpt-text">
                            	Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore <a href="#">[+more]</a> 
                            </div>
                        </div>
                        
                        <div class=" col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">
                        	<div class="case-testimonial"><span class="fa fa-quote-left"></span>
                            <blockquote>
                            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                            </blockquote>
                            <div class="testimonial-author">Samuel Doe, Xiom Corp</div></div>
                        </div>
                    
                    </div>
                    
                </div>
                <div class="case-item">
                	<figure class="client-fig wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    	<h2 class="client-name wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.4s">Xiom Corp</h2>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/case-1w.png" class="case-cover-img">
                        <div class="case-logo wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.6s"><img src="<?php echo get_template_directory_uri(); ?>/img/client-logo-1.png"></div>
                    </figure>
                    
                    
                    <div class="row case-body">
                    	<div class="case-excerpt col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
                        	<h3 class="title text-uppercase spacer-bottom-xs"><span class="fa fa-clipboard"></span> <span class="color-red">Details</span></h3>
                            <div class="case-excerpt-text">
                            	Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore <a href="#">[+more]</a> 
                            </div>
                        </div>
                        
                        <div class=" col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">
                        	<div class="case-testimonial"><span class="fa fa-quote-left"></span>
                            <blockquote>
                            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                            </blockquote>
                            <div class="testimonial-author">Samuel Doe, Xiom Corp</div></div>
                        </div>
                    
                    </div>
                    
                </div>
                <div class="case-item">
                	<figure class="client-fig wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    	<h2 class="client-name wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.4s">Xiom Corp</h2>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/case-1w.png" class="case-cover-img">
                        <div class="case-logo wow flipInY" data-wow-duration="1.5s" data-wow-delay="0.6s"><img src="<?php echo get_template_directory_uri(); ?>/img/client-logo-1.png"></div>
                    </figure>
                    
                    
                    <div class="row case-body">
                    	<div class="case-excerpt col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
                        	<h3 class="title text-uppercase spacer-bottom-xs"><span class="fa fa-clipboard"></span> <span class="color-red">Details</span></h3>
                            <div class="case-excerpt-text">
                            	Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore <a href="#">[+more]</a> 
                            </div>
                        </div>
                        
                        <div class=" col-sm-6 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">
                        	<div class="case-testimonial"><span class="fa fa-quote-left"></span>
                            <blockquote>
                            Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                            </blockquote>
                            <div class="testimonial-author">Samuel Doe, Xiom Corp</div></div>
                        </div>
                    
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

</div>




<div class="wrapper page-wrapper content-section-xs bg-red section-conversion wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="row">
        <div class="col-sm-6 text-right">
            <h3 class="section-title section-title-md title wt-bold spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">Start Converting</h3>
            <div class="font-md spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">Receive a personalised demo.</div>
        </div>
        <div class="col-sm-6"> <a href="#" class="btn btn-white-bordered wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri();?>/img/ico-play.png" class="ico"> Request a demo</a> </div>
    </div>
</div>

</div>
<?php

get_footer();
