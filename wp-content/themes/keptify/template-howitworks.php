<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 Template name: How it works
 */

get_header(); ?>

<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                <h2 class="page-title text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">How It Works</h2>
                <div class="page-slogan text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s">Built for marketers who understand that every dollar counts.</div>
                <div class="separator separator-black wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s"></div>
                <div class="font-md text-grey lh-lg text-justify wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">
                    <p>Have you ever wondered why people visit your website and don’t purchase? It’s a bummer, but we’ve found that it doesn’t mean they aren’t interested; they just need to be exposed to your message a few more times. Keptify’s suite of products including our advanced analytics tracking provides you with detailed data on potential customer's behavior. Pairing that data with our additional remarketing tools will improve your marketing results. You'll have the ability to create hyper-targeted content that delivers the right message at the right time to prospective buyers. It’s a simple non-invasive method of remarketing to potential customers.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section bg-lightgrey">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-md-5 col-md-offset-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="embed-responsive embed-responsive-16by9">
                    <div class="video-play"></div>
                    <video  id="videoId1" class="embed-responsive-item" poster="<?php echo get_template_directory_uri(); ?>/img/poster-1.png">
                        <source src="<?php echo get_template_directory_uri(); ?>/video/BigBuckBunny_512kb.mp4" type="video/mp4">
                        Your browser does not support the video tag. </video>
                </div>
            </div>
            <div class="col-lg-4 col-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title">Shopping Cart <br>
                    Analytics</h2>
                <div class="section-short-desc">As the saying goes, <strong>"you can't <br>
                    manage what you don't measure."</strong></div>
                <div class="section-desc"> Understand why your website visitors aren’t buying with our analytics software. Discover abandonment trends and indicators such as location, gender, device, browser, search history, mouse movements, purchase history, and even your most abandoned products! Our analytics data will enable you to optimize your website to increase conversion. </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-sm-offset-1 col-lg-4 col-md-5 text-right wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title">Email<br>
                    Remarketing</h2>
                <div class="section-desc">
                    <p>Let’s say a customer (we’ll name her Susie) is about to purchase your product. She's on the checkout page, fills out her name and email, but out of nowhere her sister calls her to come to the other room. As a result, she closes her computer screen, leaving the purchase page. </p>
                    <p>Previously there was no system in place to target "the Susie's". Until now, with Keptify you can capture Susie's name and email (even if she didn’t hit submit) and within minutes we’ll send an email prompting her to finish the transaction. When she returns to her computer, she’ll have a reminder to complete the purchase waiting in her inbox.</p>
                </div>
            </div>
            <div class="col-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="embed-responsive embed-responsive-16by9">
                    <div class="video-play"></div>
                    <video  id="videoId2" class="embed-responsive-item" poster="<?php echo get_template_directory_uri(); ?>/img/poster-2.png">
                        <source src="<?php echo get_template_directory_uri(); ?>/video/BigBuckBunny_512kb.mp4" type="video/mp4">
                        Your browser does not support the video tag. </video>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section bg-lightgrey">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-md-5 col-md-offset-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="embed-responsive embed-responsive-16by9">
                    <div class="video-play"></div>
                    <video id="videoId3"  class="embed-responsive-item" poster="<?php echo get_template_directory_uri(); ?>/img/poster-3.png">
                        <source src="<?php echo get_template_directory_uri(); ?>/video/BigBuckBunny_512kb.mp4" type="video/mp4">
                        Your browser does not support the video tag. </video>
                </div>
            </div>
            <div class="col-lg-4 col-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title">Lightbox <br>
                    Forms</h2>
                <div class="section-desc"> At Keptify we understand one of the most important "proverbs" of running an online business…"The money is in the list”. That’s why we help grow your email list with our elegant high-converting lightbox forms, proven to generate 500% percent more email opt-ins. Track the moment before a customer tries to leave your website, then display a form giving them one more opportunity to sign up to your list or claim a special offer. </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-lg-4 col-md-5 col-md-offset-1 text-right wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title">Automation</h2>
                <div class="section-desc">
                    <p>You're busy, and we understand that. Let us do the work for you. Keptify's automation software sends out personalized newsletters based on your customer’s interests and actions on your site. Increase open rates and engagement by sending customers relevant content.</p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="embed-responsive embed-responsive-16by9 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                    <div class="video-play"></div>
                    <video id="videoId4"  class="embed-responsive-item" poster="<?php echo get_template_directory_uri(); ?>/img/poster-4.png">
                        <source src="<?php echo get_template_directory_uri(); ?>/video/BigBuckBunny_512kb.mp4" type="video/mp4">
                        Your browser does not support the video tag. </video>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section bg-lightgrey">
    <div class="container-fluid">
        <div class="row flex-center">
            <div class="col-md-5 col-md-offset-1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="embed-responsive embed-responsive-16by9">
                    <div class="video-play"></div>
                    <video  id="videoId5" class="embed-responsive-item" poster="<?php echo get_template_directory_uri(); ?>/img/poster-5.png">
                        <source src="<?php echo get_template_directory_uri(); ?>/video/BigBuckBunny_512kb.mp4" type="video/mp4">
                        Your browser does not support the video tag. </video>
                </div>
            </div>
            <div class="col-lg-4 col-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <h2 class="section-title">Facebook & Adroll<br>
                    Remarketing </h2>
                <div class="section-desc"> Remarket to customers using popular platforms like Facebook and Adroll. Use either one to target customers that abandoned your cart. Take control over how and when your ads are displayed, and receive accurate conversion reports and analytics. </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper">
    <div class="row flex-center">
        <div class="table">
            <div class="col-sm-6 bg-darkgrey text-center table-cell">
                <div class="twoBlock-wrapper  content-section wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                    <h3 class="section-title wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s"><strong>The Problem</strong></h3>
                    <div class="separator wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s"></div>
                    <div class="section-short-desc wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">Two-thirds of your website visitors will 
                        leave without purchasing</div>
                    <figure class="spacer-bottom-md wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri(); ?>/img/problem-icon.png"></figure>
                    <div class=" wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s"><p>Even though the majority of them were
                        planning on purchasing.</p>
                    <p>That’s a lot of wasted time and effort in
                        bringing them to your site in the first place.</p></div>
                </div>
            </div>
            <div class="col-sm-6 bg-red text-center table-cell">
                <div class="twoBlock-wrapper  content-section wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                    <h3 class="section-title wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s"><strong>Our Solution</strong></h3>
                    <div class="separator wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.7s"></div>
                    <div class="section-short-desc wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.9s">Our onsite and email remarketing predicts and 
                        eacts to your customer’s behaviour</div>
                    <figure class="spacer-bottom-md wow fadeInUp" data-wow-duration="1s" data-wow-delay="2.1s"><img src="<?php echo get_template_directory_uri(); ?>/img/solution-icon.png"></figure>
                    <div class=" wow fadeInUp" data-wow-duration="1s" data-wow-delay="2.3s"><p>and serves them content that’s tailored to their unique interests
                        at the perfect moment
                        to inspire conversion </p>
                    <p> It’s the smart and simple way to drive conversions</p></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
    <h2 class="section-title text-center color-red wt-bold wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">Just some of the things our <br>
        campaigns can do</h2>
    <div class="separator separator-black wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="row">
                    <div class="col-sm-4 text-center spacer-bottom-xl wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">
                        <figure class="icon-feat spacer-bottom-sm"><img src="<?php echo get_template_directory_uri(); ?>/img/f-icon-1.png"></figure>
                        <figcaption class="wt-light">Acquire New Customers</figcaption>
                    </div>
                    <div class="col-sm-4 text-center spacer-bottom-xl wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">
                        <figure class="icon-feat spacer-bottom-sm"><img src="<?php echo get_template_directory_uri(); ?>/img/f-icon-2.png"></figure>
                        <figcaption class="wt-light">Improve Retention</figcaption>
                    </div>
                    <div class="col-sm-4 text-center spacer-bottom-xl wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s">
                        <figure class="icon-feat spacer-bottom-sm"><img src="<?php echo get_template_directory_uri(); ?>/img/f-icon-3.png"></figure>
                        <figcaption class="wt-light">Lift Average Order Value</figcaption>
                    </div>
                    <div class="col-sm-4 text-center spacer-bottom-xl wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.3s">
                        <figure class="icon-feat spacer-bottom-sm"><img src="<?php echo get_template_directory_uri(); ?>/img/f-icon-4.png"></figure>
                        <figcaption class="wt-light">Reduce Cart Abandonment</figcaption>
                    </div>
                    <div class="col-sm-4 text-center spacer-bottom-xl wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.5s">
                        <figure class="icon-feat spacer-bottom-sm"><img src="<?php echo get_template_directory_uri(); ?>/img/f-icon-5.png"></figure>
                        <figcaption class="wt-light">Upsell Specific Items</figcaption>
                    </div>
                    <div class="col-sm-4 text-center spacer-bottom-xl wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.7s">
                        <figure class="icon-feat spacer-bottom-sm"><img src="<?php echo get_template_directory_uri(); ?>/img/f-icon-6.png"></figure>
                        <figcaption class="wt-light">Fast Track path to Purchase</figcaption>
                    </div>
                </div>
                <div class="text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="2s"><a href="#" class="btn btn-red-bordered"><img src="<?php echo get_template_directory_uri();?>/img/ico-play-red.png" class="ico"> Request a demo</a></div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section text-canter bg-container color-white text-center wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bg-dollar.png);">
    <h3 class="section-title wt-bold wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">Case Studies</h3>
    <div class="separator wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s"></div>
    <img class="case-img wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s" src="<?php echo get_template_directory_uri();?>/img/case-1.png">
    <div class="section-short-desc spacer-top-md wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">10:1 Return<br>
        on investment</div>
    <a href="#" class="btn btn-white-bordered wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri();?>/img/ico-download.png" class="ico"> Download Case Study</a></div>
<div class="wrapper page-wrapper content-section-xs bg-red section-conversion wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="row">
        <div class="col-sm-6 text-right">
            <h3 class="section-title section-title-md title wt-bold spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">Start Converting</h3>
            <div class="font-md spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">Receive a personalised demo.</div>
        </div>
        <div class="col-sm-6"> <a href="#" class="btn btn-white-bordered wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri();?>/img/ico-play.png" class="ico"> Request a demo</a> </div>
    </div>
</div>
</div>
<?php

get_footer();

