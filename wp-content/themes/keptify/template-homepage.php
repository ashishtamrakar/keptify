<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 Template name: Home page
 */

get_header(); ?>

	<div class="section-group"> <img src="<?php echo get_template_directory_uri();?>/img/home-top-1.jpg" class="section-bg"> <img src="<?php echo get_template_directory_uri();?>/img/home-top-2.jpg" class="section-bg section-bg-bottom">
    <section class="home-section home-section-1">
        <div class="main-block wow fadeInDown"   data-wow-duration="1.5s" data-wow-delay="0.3s">
            <h1  class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.9s">Convert Website Visitors <br>
                into Customers</h1>
            <div class="sub-header wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.8s">Increase your revenue potential by remarketing to potential buyers</div>
            <div class="separator separator-white  wow  fadeInDown"  data-wow-duration="1s" data-wow-delay="0.7s"></div>
            <div class="actions  wow  fadeInDown"  data-wow-duration="1s" data-wow-delay="0.5s"> 
            <a href="#" class="btn btn-white-bordered"><img src="<?php echo get_template_directory_uri();?>/img/ico-play.png" class="ico"> Request a demo</a> 
            <a href="#" class="btn btn-red"><img src="<?php echo get_template_directory_uri();?>/img/ico-user.png" class="ico"> Sign UP</a> </div>
        </div>
       <div class=" wow fadeInDown next-block-container"  data-wow-duration="1s" data-wow-delay="0.5s"> <a href="#home-section-2" class="next-block lscroll element-animation"><span>Learn More</span></a></div> </section>
       
    <section class="home-section home-section-2" id="home-section-2">
        <div class="main-block">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.3s"><img src="<?php echo get_template_directory_uri();?>/img/ico-problem.png" class="icon-title wow fadeInDown"  data-wow-duration="1s" data-wow-delay="1.1s">
                        <h1 class="wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.9s">The Problem</h1>
                        <div class="separator separator-white wow fadeInDown" data-wow-duration="1s" data-wow-delay="1.1s"></div>
                        <p class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s"> A staggering 70% of checkout carts get abandoned before the shopper completes their purchase. Every day businesses are missing out on hundreds, even thousands of dollars because they don't follow up with potential customers. Studies show that 35% of shoppers who received an email from the merchant after abandoning, returned to complete their order.</p>
                    </div>
                </div>
            </div>
        </div>
      <div class="wow fadeInDown next-block-container"  data-wow-duration="1s" data-wow-delay="0.5s"> <a href="#home-section-3" class="next-block lscroll"><span>Our solution</span></a></div> </section>
    <section class="home-section home-section-3" id="home-section-3">
        <div class="main-block">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 wow fadeInDown"   data-wow-duration="1s" data-wow-delay="0.3s"> <img src="<?php echo get_template_directory_uri();?>/img/ico-solution.png" class="icon-title wow fadeInDown"  data-wow-duration="1s" data-wow-delay="1.1s">
                        <h1 class=" wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.9s">Our Solution</h1>
                        <div class="separator separator-white wow fadeInDown"  data-wow-duration="1s" data-wow-delay="1.1s"></div>
                        
                        <p  class="wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.5s"> A staggering 70% of checkout carts get abandoned before the shopper completes their purchase. Every day businesses are missing out on hundreds, even thousands of dollars because they don't follow up with potential customers. Studies show that 35% of shoppers who received an email from the merchant after abandoning, returned to complete their order.</p>
                        <div class="actions"> <a href="#" class="btn btn-white-bordered"><img src="<?php echo get_template_directory_uri();?>/img/ico-play.png" class="ico"> Request a demo</a> <a href="#" class="btn btn-red"><img src="<?php echo get_template_directory_uri();?>/img/ico-user.png" class="ico"> Sign UP</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section class="features">
    <div class="features-wrapper">
        <div class="row">
            <div class="col-sm-4 text-center">
                <div class="feat-item wow fadeInDown"  data-wow-duration="1.5s" data-wow-delay="0.1s">
                <img class=" wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.9s" src="<?php echo get_template_directory_uri();?>/img/ico-cart.png">
                    <div class="separator separator-sm wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.7s"></div>
                    <div class="feat-bottom">
                        <h4 class="font-md title wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.7s">Advanced Cart Analyzer</h4>
                        <p class="wt-light wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.5s">We analyze and understand your users by key abandonment indicators such as location, gender, device and browser</p>
                    </div>
                    <a href="#" class="btn btn-white-bordered btn-xs wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.3s">Read whitepaper</a> </div>
            </div>
            <div class="col-sm-4 text-center">
                <div class="feat-item wow fadeInDown"  data-wow-duration="1.5s" data-wow-delay="0.1s">
                <img class=" wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.9s" src="<?php echo get_template_directory_uri();?>/img/ico-testimonia.png">
                    <div class="separator separator-sm wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.7s"></div>
                    <div class="feat-bottom">
                        <blockquote class="feat-quote wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.5s">“These guys are great, really proactive and just make it easy for us. ”</blockquote>
                        <div class="author wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.4s">
                            <div class="font-md">Eugen Rus</div>
                            <div class="font-sm">Sales Director, monplatin.ro</div>
                        </div>
                    </div>
                    <a href="#" class="btn btn-white-bordered btn-xs wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.3s">Read whitepaper</a> </div>
            </div>
            <div class="col-sm-4 text-center">
                <div class="feat-item wow fadeInDown"  data-wow-duration="1.5s" data-wow-delay="0.1s">
                <img class=" wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.9s" src="<?php echo get_template_directory_uri();?>/img/ico-search.png">
                    <div class="separator separator-sm wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.7s"></div>
                    <div class="feat-bottom">
                        <div class="casehome-title font-md wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.7s"><strong>PizzaHut</strong></div>
                        <div class="casehome-return wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.5s">99:1 Return<br>
                            on investment</div>
                        <a href="#" class="casehome-link"><span class="fa fa-cloud-download wow fadeInDown" data-wow-duration="1s" data-wow-delay="0.4s"></span> Download Case Study</a></div>
                    <a href="#" class="btn btn-white-bordered btn-xs wow fadeInDown"  data-wow-duration="1s" data-wow-delay="0.3s">Read whitepaper</a> </div>
            </div>
        </div>
    </div>
</section>

<?php

get_footer();
