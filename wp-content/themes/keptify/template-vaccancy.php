<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 Template name: Vaccancy
 */

get_header(); ?>

<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                <h2 class="page-title text-center">Vacancies  </h2>
                <div class="page-subtitle text-center">Join a good company</div>
                <div class="separator separator-black"></div>
                <div class="pager-title-content text-center">
                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p> </div>

<div class="separator separator-black"></div>



<div class="vacancy-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
	
    <div class="vacancy-fig"><img src="<?php echo get_template_directory_uri(); ?>/img/vac-img.png"></div>
    <div class="vacancy-body">
    	<div class="no-of-pos">2 pos</div>
        <h3 class="vacancy-post">Digital Marketing Executive</h3>
        <h4 class="vacany-subtitle">Educational Qualification:</h4>
<div class="vacany-value">Bachelors in related field</div>
    
    
    
    <h4 class="vacany-subtitle">Qualifications:</h4>
<div class="vacany-value">Affection with e­commerce/ webshops<br>
Fluent in English language, both written and spoken is a must<br>
Quick learner</div>
    </div>

</div>
<div class="vacancy-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
	
    <div class="vacancy-fig"><img src="<?php echo get_template_directory_uri(); ?>/img/vac-img2.png"></div>
    <div class="vacancy-body">
    	<div class="no-of-pos">2 pos</div>
        <h3 class="vacancy-post">Digital Marketing Executive</h3>
        <h4 class="vacany-subtitle">Educational Qualification:</h4>
<div class="vacany-value">Bachelors in related field</div>
    
    
    
    <h4 class="vacany-subtitle">Qualifications:</h4>
<div class="vacany-value">Affection with e­commerce/ webshops<br>
Fluent in English language, both written and spoken is a must<br>
Quick learner</div>
    </div>

</div>
<div class="vacancy-item wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
	
    <div class="vacancy-fig"><img src="<?php echo get_template_directory_uri(); ?>/img/vac-img3.png"></div>
    <div class="vacancy-body">
    	<div class="no-of-pos">2 pos</div>
        <h3 class="vacancy-post">Digital Marketing Executive</h3>
        <h4 class="vacany-subtitle">Educational Qualification:</h4>
<div class="vacany-value">Bachelors in related field</div>
    
    
    
    <h4 class="vacany-subtitle">Qualifications:</h4>
<div class="vacany-value">Affection with e­commerce/ webshops<br>
Fluent in English language, both written and spoken is a must<br>
Quick learner</div>
    </div>

</div>
<div class="vacancy-item vacancy-item-more wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
	
    <div class="vacancy-fig"><img src="<?php echo get_template_directory_uri(); ?>/img/vac-img-plus.png"></div>
    <div class="vacancy-body">
   
    
    
    <h4 class="vacany-subtitle spacer-top-null">More comming soon</h4>
<div class="vacany-value">Subscribe to oour news letter o stay tuned</div>


<div class="input-group spacer-top-sm">
      
      <input type="text" class="form-control" placeholder="email address">
      <span class="input-group-btn">
        <button class="btn btn-red" type="button">Stay tuned</button>
      </span>
    </div>

    </div>

</div>

                
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section-xs bg-red section-conversion wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
    <div class="row">
        <div class="col-sm-6 text-right">
            <h3 class="section-title section-title-md title wt-bold spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">Start Converting</h3>
            <div class="font-md spacer-top-xs wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">Receive a personalised demo.</div>
        </div>
        <div class="col-sm-6"> <a href="#" class="btn btn-white-bordered wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri();?>/img/ico-play.png" class="ico"> Request a demo</a> </div>
    </div>
</div>

<div class="wrapper page-wrapper content-section text-canter bg-container color-white text-center wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bg-dollar.png);">
    <h3 class="section-title wt-bold wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">Case Studies</h3>
    <div class="separator wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.5s"></div>
    <img class="case-img wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s" src="<?php echo get_template_directory_uri();?>/img/case-1.png">
    <div class="section-short-desc spacer-top-md wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.9s">10:1 Return<br>
        on investment</div>
    <a href="#" class="btn btn-white-bordered wow fadeInUp" data-wow-duration="1s" data-wow-delay="1.1s"><img src="<?php echo get_template_directory_uri();?>/img/ico-download.png" class="ico"> Download Case Study</a></div>
</div>
<?php

get_footer();


