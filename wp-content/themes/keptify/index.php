<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 */

get_header(); ?>

<div id="primary" class="content-area">
<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.3s">
                <h2 class="page-title  text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">BLOG</h2>
                <div class="separator separator-black wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s"></div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-sm-7 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                <div class="blog-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="row">
                        <div class="col-md-4"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-1.png" class="img-responsive"></div>
                        <div class="col-md-8">
                            <h3 class="blog-title">This is a Standard post with a Preview Image</h3>
                            <div class="blog-meta text-muted">
                                <div class="blog-meta-item">Author: keptify</div>
                                <div class="blog-meta-item">10th Feb 2014</div>
                                <div class="blog-meta-item"> 0 Comments</div>
                            </div>
                            <div class="blog-excerpt">
                                <p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus </p>
                            </div>
                            <a href="#" class="color-red">Read more <span class="fa fa-angle-right"></span></a> </div>
                    </div>
                </div>
                <div class="blog-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="row">
                        <div class="col-md-4"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-1.png" class="img-responsive"></div>
                        <div class="col-md-8">
                            <h3 class="blog-title">This is a Standard post with a Preview Image</h3>
                            <div class="blog-meta text-muted">
                                <div class="blog-meta-item">Author: keptify</div>
                                <div class="blog-meta-item">10th Feb 2014</div>
                                <div class="blog-meta-item"> 0 Comments</div>
                            </div>
                            <div class="blog-excerpt">
                                <p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus </p>
                            </div>
                            <a href="#" class="color-red">Read more <span class="fa fa-angle-right"></span></a> </div>
                    </div>
                </div>
                <div class="blog-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="row">
                        <div class="col-md-4"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-1.png" class="img-responsive"></div>
                        <div class="col-md-8">
                            <h3 class="blog-title">This is a Standard post with a Preview Image</h3>
                            <div class="blog-meta text-muted">
                                <div class="blog-meta-item">Author: keptify</div>
                                <div class="blog-meta-item">10th Feb 2014</div>
                                <div class="blog-meta-item"> 0 Comments</div>
                            </div>
                            <div class="blog-excerpt">
                                <p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus </p>
                            </div>
                            <a href="#" class="color-red">Read more <span class="fa fa-angle-right"></span></a> </div>
                    </div>
                </div>
                <div class="blog-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="row">
                        <div class="col-md-4"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-1.png" class="img-responsive"></div>
                        <div class="col-md-8">
                            <h3 class="blog-title">This is a Standard post with a Preview Image</h3>
                            <div class="blog-meta text-muted">
                                <div class="blog-meta-item">Author: keptify</div>
                                <div class="blog-meta-item">10th Feb 2014</div>
                                <div class="blog-meta-item"> 0 Comments</div>
                            </div>
                            <div class="blog-excerpt">
                                <p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus </p>
                            </div>
                            <a href="#" class="color-red">Read more <span class="fa fa-angle-right"></span></a> </div>
                    </div>
                </div>
                <div class="blog-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="row">
                        <div class="col-md-4"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-1.png" class="img-responsive"></div>
                        <div class="col-md-8">
                            <h3 class="blog-title">This is a Standard post with a Preview Image</h3>
                            <div class="blog-meta text-muted">
                                <div class="blog-meta-item">Author: keptify</div>
                                <div class="blog-meta-item">10th Feb 2014</div>
                                <div class="blog-meta-item"> 0 Comments</div>
                            </div>
                            <div class="blog-excerpt">
                                <p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus </p>
                            </div>
                            <a href="#" class="color-red">Read more <span class="fa fa-angle-right"></span></a> </div>
                    </div>
                </div>
                <div class="blog-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="row">
                        <div class="col-md-4"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-1.png" class="img-responsive"></div>
                        <div class="col-md-8">
                            <h3 class="blog-title">This is a Standard post with a Preview Image</h3>
                            <div class="blog-meta text-muted">
                                <div class="blog-meta-item">Author: keptify</div>
                                <div class="blog-meta-item">10th Feb 2014</div>
                                <div class="blog-meta-item"> 0 Comments</div>
                            </div>
                            <div class="blog-excerpt">
                                <p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus </p>
                            </div>
                            <a href="#" class="color-red">Read more <span class="fa fa-angle-right"></span></a> </div>
                    </div>
                </div>
                <div class="blog-item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="row">
                        <div class="col-md-4"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-1.png" class="img-responsive"></div>
                        <div class="col-md-8">
                            <h3 class="blog-title">This is a Standard post with a Preview Image</h3>
                            <div class="blog-meta text-muted">
                                <div class="blog-meta-item">Author: keptify</div>
                                <div class="blog-meta-item">10th Feb 2014</div>
                                <div class="blog-meta-item"> 0 Comments</div>
                            </div>
                            <div class="blog-excerpt">
                                <p>Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus </p>
                            </div>
                            <a href="#" class="color-red">Read more <span class="fa fa-angle-right"></span></a> </div>
                    </div>
                </div>
                
                
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li> <a href="#" aria-label="Next"> <span aria-hidden="true">Next page</span> </a> </li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-3 col-md-offset-1 col-sm-5 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">
                <?php get_sidebar(); ?>
                <!--<section class="widget">
                    <h2 class="widget-title">Latest Posts</h2>
                    <div class="border-bottom sidebar-item font-sm latest-post-item">
                    	<a href="#" class="media">
                        	<div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/latest-post-1.png" class="media-object"></div>
                            <div class="media-body">
                            	<time class="text-muted spacer-bottom-sm">3 days ago</time>
                                <div class="latest-post-excerpt">Styled Themes – A new and improved site with ease in navigation!</div>
                            </div>
                        </a>
                    </div>
                    <div class="border-bottom sidebar-item font-sm latest-post-item">
                    	<a href="#" class="media">
                        	<div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/latest-post-1.png" class="media-object"></div>
                            <div class="media-body">
                            	<time class="text-muted spacer-bottom-sm">3 days ago</time>
                                <div class="latest-post-excerpt">Styled Themes – A new and improved site with ease in navigation!</div>
                            </div>
                        </a>
                    </div>
                    <div class="border-bottom sidebar-item font-sm latest-post-item">
                    	<a href="#" class="media">
                        	<div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/latest-post-1.png" class="media-object"></div>
                            <div class="media-body">
                            	<time class="text-muted spacer-bottom-sm">3 days ago</time>
                                <div class="latest-post-excerpt">Styled Themes – A new and improved site with ease in navigation!</div>
                            </div>
                        </a>
                    </div>
                </section>-->
                
                <section class="widget">
                    <h2 class="widget-title">Latest Comments</h2>
                    <div class="border-bottom sidebar-item font-sm latest-comment-item"> <a href="#" class="media">
                        <div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/author-1.png" class="media-object"></div>
                        <div class="media-body">
                            <div class="row spacer-bottom-sm">
                                <div class="author-name col-xs-6">Coco</div>
                                <time class="text-muted col-xs-6 text-right">April 7, 2016</time>
                            </div>
                            <div class="latest-post-excerpt"><em>"Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam…"</em></div>
                        </div>
                        </a> </div>
                    <div class="border-bottom sidebar-item font-sm latest-comment-item"> <a href="#" class="media">
                        <div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/author-1.png" class="media-object"></div>
                        <div class="media-body">
                            <div class="row spacer-bottom-sm">
                                <div class="author-name col-xs-6">Coco</div>
                                <time class="text-muted col-xs-6 text-right">April 7, 2016</time>
                            </div>
                            <div class="latest-post-excerpt"><em>"Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam…"</em></div>
                        </div>
                        </a> </div>
                    <div class="border-bottom sidebar-item font-sm latest-comment-item"> <a href="#" class="media">
                        <div class="media-left spacer-right-md"><img src="<?php echo get_template_directory_uri(); ?>/img/author-1.png" class="media-object"></div>
                        <div class="media-body">
                            <div class="row spacer-bottom-sm">
                                <div class="author-name col-xs-6">Coco</div>
                                <time class="text-muted col-xs-6 text-right">April 7, 2016</time>
                            </div>
                            <div class="latest-post-excerpt"><em>"Nullam viverra consectetuer. Quisque cursus et, porttitor risus. Aliquam…"</em></div>
                        </div>
                        </a> </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!-- #primary -->

<?php

get_footer();


