<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package keptify
 Template name: Pricing
 */

get_header(); ?>

<div class="wrapper page-wrapper content-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="page-title text-center">Pricing </h2>
                <div class="font-lg text-center wt-semi"><em>Pay When You Get Results<br>
(the risk is on us)</em></div>
                <div class="separator separator-black"></div>
                <div class="pager-title-content">
                   <p> Our price is the most competitive in the market. We are confident in our product, so much so that we’re willing to bet on it. Don’t pay us a dime until you start recouping money, in other words; you pay for performance. </p>

<h3 class="title">How it works:</h3>
<p>Our software enables us to track website visitors behavior. If a customer returns and purchases your product because of our remarketing tools we’ll take a small percentage of the sale.</p>

<p>Only pay when we start working for you, so there’s no risk!</p>
                    <a href="#" class="btn btn-red btn-fixed-width"><img src="<?php echo get_template_directory_uri(); ?>/img/ico-user.png" class="ico"> Sign-up For Free</a> </div>
                
            </div>
        </div>
    </div>
</div>
<div class="wrapper page-wrapper content-section-xs bg-red section-conversion">
    <div class="row">
        <div class="col-sm-6 text-right">
            <h3 class="section-title section-title-md title wt-bold spacer-top-xs">Start Converting</h3>
            <div class="font-md spacer-top-xs">Receive a personalised demo.</div>
        </div>
        <div class="col-sm-6"> <a href="#" class="btn btn-white-bordered"><img src="<?php echo get_template_directory_uri();?>/img/ico-play.png" class="ico"> Request a demo</a> </div>
    </div>
</div>

<div class="wrapper page-wrapper content-section text-canter bg-container color-white text-center" style="background-image:url(<?php echo get_template_directory_uri();?>/img/bg-dollar.png);">
<h3 class="section-title wt-bold">Case Studies</h3>
<div class="separator"></div>
<img class="case-img" src="<?php echo get_template_directory_uri();?>/img/case-1.png">
<div class="section-short-desc spacer-top-md">10:1 Return<br>
on investment</div>

<a href="#" class="btn btn-white-bordered"><img src="<?php echo get_template_directory_uri();?>/img/ico-download.png" class="ico"> Download Case Study</a></div>
</div>
<?php

get_footer();


