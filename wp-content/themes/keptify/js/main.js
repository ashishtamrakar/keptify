jQuery(document).ready(function($){
	new WOW().init();
	window.onscroll = function (e) {
		var top = window.pageYOffset || document.documentElement.scrollTop;
		if (top >= 300) {
			jQuery('.site-header').addClass('scrolled');
		} else {
			jQuery('.site-header').removeClass('scrolled');
		}
	}
	
	var fbmax=0;
	$('.feat-bottom').each(function(){
		if($(this).height()>fbmax){
			fbmax=$(this).height();
		}
	})
	$('.feat-bottom').height(fbmax);
	
	
	$('.lscroll').click(function () {
		var targetID=$(this).attr('href');
		console.log(targetID);
		$('html, body').animate({
			scrollTop:$(targetID).position().top
		}, 800, 'easeOutExpo');
		return false;
		});
	
	$('.video-play').toggle(
		function(){
			$(this).closest('.embed-responsive').find('video').get(0).play();
		},
		function(){
			$(this).closest('.embed-responsive').find('video').get(0).pause()
		}
	)
	
	$('.main-navigaion li.login a, .main-navigaion li.signup a').fancybox({wrapCSS:'keptify-pop', closeBtn:false, helpers : {
        overlay : {
            css : {
                'background' : 'rgba(111, 124, 127, 0.95)'
            }
        }
    }});
});