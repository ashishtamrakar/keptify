<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package keptify
 */

?>

	</div><!-- #content -->

	
</div><!-- #page -->
<footer class="wrapper site-footer">
    <div class="container-fluid wow fadeIn"  data-wow-duration="1.5s" data-wow-delay="0.5s">
        <div class="row">
            <div class="col-sm-2 col-sm-offset-1">
                <h5 class="font-nm title footer-menu-title">Keptify</h5>
                <ul class="footer-menu">
                    <li><a href="#">Partners</a></li>
                    <li><a href="#">About us</a></li>
                    <li><a href="#">Vacancies</a></li>
                    <li><a href="#">Privacy policy</a></li>
                    <li><a href="#">Cookie policy</a></li>
                    <li><a href="#">Terms and conditions</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5 class="font-nm title footer-menu-title">MENU</h5>
                <ul class="footer-menu">
                    <li><a href="#">How it works</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Case studies</a></li>
                    <li><a href="#">Pricing</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Contact us</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5 class="font-nm title footer-menu-title">Blogs</h5>
                <ul class="footer-menu">
                    <li><a href="#">Request a Demo</a></li>
                    <li><a href="#">Our Products</a></li>
                    <li><a href="#">Our Clients</a></li>
                    <li><a href="#">How It Works</a></li>
                    <li><a href="#">Resources</a></li>
                    <li><a href="#">Careers</a></li>
                </ul>
            </div>
            <div class="col-sm-2">
                <h5 class="font-nm title footer-menu-title">Contact INFO</h5>
                <ul class="footer-menu">
                    <li><span class="fa fa-send"></span>hello@keptify.com</li>
                    <li><span class="fa fa-phone"></span>+007 911 198 123</li>
                    <li><span class="fa fa-map-marker"></span>Heeze, Netherlands</li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5 class="font-nm title footer-menu-title">Social</h5>
                <div class="footer-socials"><a href="#"><span class="fa fa-facebook-square"></span></a> <a href="#"><span class="fa fa-twitter-square"></span></a> <a href="#"><span class="fa fa-google-plus-square"></span></a> <a href="#"><span class="fa fa-linkedin-square"></span></a></div>
                <a href="#" class="fooer-logo"><img src="<?php echo get_template_directory_uri();?>/img/keptify-red-footer.png"></a> </div>
        </div>
    </div>
</footer>
<div class="hidden">

<div class="signup-content" id="signup">
	
	<div class="text-center"><h3 class="pop-title">Sign up</h3>
	<p>Once you join Keptify, our team of experts will take care of the design and setup of your campaigns. We'll keep you in the loop by sending you progress updates.</p>

<p>Get started by filling out the form below:</p> </div>


<div class="signup-form-container">
	
    <div class="row gutter-sm spacer-bottom-sm">
    	<div class="col-sm-6 gutter-sm"><input type="text" class="form-control" value="" placeholder="First name"></div>
    	<div class="col-sm-6 gutter-sm"><input type="text" class="form-control" value="" placeholder="Last name"></div>
    </div>
<div class="row spacer-bottom-sm">
    	<div class="col-sm-12"><input type="text" class="form-control" value="" placeholder="Username"></div>
    </div>
<div class="row spacer-bottom-sm">
    	<div class="col-sm-12"><input type="text" class="form-control" value="" placeholder="Email address"></div>
    </div>
    <div class="row spacer-bottom-sm">
    	<div class="col-sm-12"><input type="text" class="form-control" value="" placeholder="Password"></div>
    </div>
    <div class="row spacer-bottom-sm">
    	<div class="col-sm-12"><input type="checkbox" class="checkbox"> I agree to terms and conditions</div>
    </div>
    <div class="text-center">
    	<input type="submit" class="btn btn-red btn-wide" value="SIgn UP">
    </div>
</div>


</div>

</div>
<?php wp_footer(); ?>

</body>
</html>
